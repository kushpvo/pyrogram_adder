import time

from os import listdir
from os.path import isfile, join

from pyrogram import Client
from pyrogram.errors import UserAlreadyParticipant, FloodWait, UserDeactivatedBan

def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n): 
        yield l[i:i + n]


clients = []
onlyfiles = [f for f in listdir("clients") if isfile(join("clients", f))]
for each in onlyfiles:
    clients.append(each.split(".",1)[0])
print("No of clients: " + str(len(clients)))


target = input("Target group: ")
# target = "https://t.me/joinchat/TgxVJ3znM6A8J3dt"
app = Client("+919609348530")
group_list = []
with app:
    try:
        target_id = app.join_chat(target).id
    except UserAlreadyParticipant as e:
        target_id = app.get_chat(target).id

    for member in app.iter_chat_members(target_id):
        if member.user.is_bot == True:
            continue
        if member.user.username == None:
            continue
        else:
            username = "@" + str(member.user.username)
            group_list.append(username)

users_file = "users.txt"
user_list = []
with open(users_file, 'r') as f:
    for line in f:
        user_list.append(line.rstrip())


output = []
for user_elm in user_list:
    save = True
    for group_elm in group_list:
        if user_elm == group_elm:
            save = False
            break
    if save == True:
        output.append(user_elm)

users_to_add = list(divide_chunks(output, 50))



for idx, client in enumerate(clients):
    try:
        app = Client(session_name=client, workdir="clients/")
        with app:
            print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + "Starting client: " + str(client))
            try:
                target_id = app.join_chat(target).id
                print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + "Joined group, sleeping for 5 seconds...")
                time.sleep(5)
            except UserAlreadyParticipant as e:
                target_id = app.get_chat(target).id


            try:
                final_list = list(divide_chunks(users_to_add[idx], 10))
                for each_list in final_list:
                    try:
                        app.add_chat_members(chat_id=target_id, user_ids=each_list)
                        print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + str(client) + ": Added! Going to next..")
                        time.sleep(1)
                        continue
                    except Exception as e:
                        print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + "CANNOT ADD TO GROUP.. SKIPPING: " + str(e))
                        app.stop()
                        break

            except Exception as e:
                print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + "CANNOT ADD TO GROUP.. SKIPPING: " + str(e))
                app.stop()
                continue

    except UserDeactivatedBan as e:
        print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + str(client) + " : USER DEACTIVATED.. SKIPPING" + str(e))
        continue

    except Exception as e:
        print("[" + str(idx + 1) + "/" + str(len(clients)) + "]" + "SOMETHING WENT WRONG.. SKIPPING" + str(e))
        continue
